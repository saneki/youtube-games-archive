YouTube Horror Game Archive
===========================

This is an archival project for preserving old horror games which were played on YouTube during the
early YouTube horror boom, especially games released in and before 2014.

This effort is currently focused on games which were provided using custom domains and file sharing
websites, many of which no longer work. Some of these downloads are still available today, some have
thankfully been archived by [Archive.org], but others have likely been lost to time.

Games uploaded on indie game websites such as [GameJolt] and [IndieDB] are usually still available
on these platforms and aren't at risk of disappearing in the immediate future, and thus they aren't
the main focus *yet*.

All game files which have been archived up to this point are available [here][Drive].

[Archive.org]:https://archive.org
[Drive]:https://drive.google.com/drive/folders/1sl8v8ojZAnKQc02-JrrkVuR69n-3xGYF
[GameJolt]:https://gamejolt.com
[IndieDB]:https://www.indiedb.com
