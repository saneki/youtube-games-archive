TITLES          = titles.yml
YAMLLINT_CONFIG = .yamllint-config.yml

YAMLLINT = yamllint

all: lint

lint:
	$(YAMLLINT) -c $(YAMLLINT_CONFIG) $(TITLES)
